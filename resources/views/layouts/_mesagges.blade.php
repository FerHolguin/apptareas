@if(Session::get('info'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>¡Atención!</strong> {{Session::get('info')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        <span aria-hidden="true">&times;</span>
    </div>
@endif

@if(Session::get('alert'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>¡Alerta!</strong> {{Session::get('alert')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        <span aria-hidden="true">&times;</span>
    </div>
@endif

@if(Session::get('exito'))
<div class="alert alert-info alert-dismissible fade show" role="alert">
        <strong>¡Exito!</strong> {{Session::get('exito')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        <span aria-hidden="true">&times;</span>
    </div>
@endif
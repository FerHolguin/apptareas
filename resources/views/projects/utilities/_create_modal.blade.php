<!-- Modal -->
<div class="modal fade" id="ModalProyectos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Proyecto</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form method="POST" action="{{ route ('proyectos.store') }}">
          {{csrf_field() }}
      <div class="modal-body">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control" name="name" required="">
                </div>
            </div>
            <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Estado</label>
                        <select name="status" class="form-control">
                            <option value="En Proceso">En Proceso</option>
                            <option value="Terminado">Terminado</option>
                            <option value="Atrasado">Atrasado</option>
                            <option value="Cancelado">Cancelado</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="">Descripcion</label>
                <textarea class="form-control" name="description" rows="3"></textarea>
            </div>

            <div class="form-group">
                <label for="SelectUserId">Selecciona usuario</label>
                    <select class="form-control" id="SelectUserId" name="user_id" multiple="">
                        @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary">Guardar Proyecto</button>
      </div>
    </div>
  </div>
</div>